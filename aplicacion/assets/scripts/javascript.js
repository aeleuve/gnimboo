$( document ).ready(function() {
  //check if code field is empty.
  var formu = $('#form_download');


  //submit
  formu.submit(function(e){
    var code_field = $('#d_code').val();
    if (code_field !== '') return;

    var itemErr = $('<span></span>');
    itemErr.attr({id:'error', class:'error'});
    itemErr.html('The field must contain a code');
    $(this).after(itemErr);
    setTimeout(function() { 
      itemErr.fadeOut('slow', function(){
        $(this).remove();
      });

    }, 2000); 
    
    e.preventDefault();
  });


  //btn descarga rollovers.
  $('.btn_descarga').mouseover(function(){
  	$(this).css('cursor','pointer');
  	$(this).addClass('btn_descarga_roll').removeClass('btn_descarga');
    $(this).unbind('click');


  	$(this).click(function(){
  		var id_img = $(this).prev().attr('rel');
  		//$('#images_wrap').load('detail.php');
  		//$('#images_wrap').html('Downloading...'); // Show "Downloading..."
			// Do an ajax request
		$.ajax({
		  type: 'POST',
		  url: base_url+"home/detalle",
		  data: 'id='+id_img
		}).done(function(data) { // data what is sent back by the php page
      var dial = $('<div></div>');
      dial.attr({id:'dialog'});
      $('#images_wrap').before(dial);
      //$('#images_wrap').html();
		  $('#dialog').html(data); // display data dialog box.

      dialog = $( "#dialog" ).dialog({
          closeOnEscape: true,
          width: '50%',
          modal: true,
          resizable: false,
          position: { 
            my: "top", 
            at: "top+150"},
          show: {
            effect: "blind",
            duration: 1000
          },
          buttons: [
            {
                // text: none,
                "class": 'btn btn-default btn-lg',
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                // text: none,
                "class": 'btn btn-default btn-lg',
                click: function() {
                    downloadpic();
                }
            }
          ],
          create: function(){
             var buttons = $('.ui-dialog-buttonset').children('button');
             buttons.removeClass("ui-button ui-widget ui-state-default ui-state-active ui-state-focus");

             $('.ui-dialog-buttonset :nth-child(1)').children().removeClass('ui-button-text').addClass('glyphicon glyphicon-remove');
             $('.ui-dialog-buttonset :nth-child(2)').children().removeClass('ui-button-text').addClass('glyphicon glyphicon-cloud-download');
          },

          close: function(){
            $('#dialog').remove();
          }
        });      

		});
		  	});

  });

  function downloadpic(){
    alert ('funcion para descargar imagen, por implementar.');
  };

  $('.btn_descarga').mouseleave(function(){
  	$(this).css('cursor','default');
  	$(this).addClass('btn_descarga').removeClass('btn_descarga_roll');
  });

});