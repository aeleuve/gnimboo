$(function() {
    initCanvas();
});

function initCanvas(){
var n_canvas = $('.thumbs');

    n_canvas.each(function(){
        var idcanvas = $(this).attr('rel');
        var tagname = "canvas_"+idcanvas;
        var canvas = document.getElementById(tagname);

        var ctx = canvas.getContext("2d");
        var imagen = $('#canvas_'+idcanvas).attr('rev');  

        img=new Image();
        img.onload=function(){
            var sourceX = 280;
            var sourceY = 290;
            var sourceWidth = this.width * 0.25;
            var sourceHeight = this.height * 0.25;
            var destWidth = sourceWidth;
            var destHeight = sourceHeight;
            var destX = canvas.width / 2 - destWidth / 2;
            var destY = canvas.height / 2 - destHeight / 2;

            ctx.drawImage(this, sourceX, sourceY, sourceWidth, sourceHeight, destX, destY, destWidth, destHeight);
            // ctx.drawImage(this, 0, 0, this.width * 0.4, this.height * 0.4);
        }
        img.src = base_url_uspath+'/'+imagen;
    });    
}