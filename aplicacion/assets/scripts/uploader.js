$(document).ready(function(){
// call initialization file
if (window.File && window.FileList && window.FileReader) {
  Init();
}
});

// initialize
function Init() {
  var fileselect = $id("fileselect"),
      filedrag = $id("filedrag"),
      submitbutton = $id("submitbutton");

  // file select
  fileselect.addEventListener("change", FileSelectHandler, false);

  // is XHR2 available?
  var xhr = new XMLHttpRequest();
  if (xhr.upload) {
    // file drop
    filedrag.addEventListener("dragover", FileDragHover, false);
    filedrag.addEventListener("dragleave", FileDragHover, false);
    filedrag.addEventListener("drop", FileSelectHandler, false);

    filedrag.style.display = "block";
    
    // remove submit button
    submitbutton.style.display = "none";
  }

   //stop submit action.
  $('#upload').submit(function( event ) {
    if (validate()) $(this).submit();
    event.preventDefault();
  });


}


function ParseFile(file) {

// display an image
  if (file.type.indexOf("image") == 0) {
    var reader = new FileReader();
    reader.onload = function(e) {
      var img = document.createElement('img'),
          fieldreference=document.getElementById('contenDrag');;

      img.setAttribute('src', e.target.result);

      insertAfter(fieldreference, img);
    }
    reader.readAsDataURL(file);
  }

  Output(
    "<p>File information: <strong>" + file.name +
    "</strong> type: <strong>" + file.type +
    "</strong> size: <strong>" + file.size +
    "</strong> bytes</p>" + 
    '<input name="image" id="image" type="hidden" value="'+ file.name +'" />' +
    '<input name="tipo" id="tipo" type="hidden" value="'+ file.type +'" />' +
    '<input name="f_size" id="f_size" type="hidden" value="'+ file.size +'" />' 
  );
  
}

function validate(){
  var codigo = $('#codigos').val(),
      msg = $('#messages_err');
  if (codigo == 0){
    //scroll to the element.
    $('html, body').animate({
      scrollTop: ($('#messages_err').offset().top)
    },500);

      msg.html('Debes seleccionar un código').delay(6000).fadeOut(400, function(){
        msg.html('');
        msg.css('display','block');
      });
  }else return true;

}

//send files to php
function SendData(file, btn){
  var uri = "http://gnimboo/admin/upload/";
  var xhr = new XMLHttpRequest();
  var fd = new FormData();

  xhr.open("POST", uri, true);
  xhr.onreadystatechange = function() {
      if (xhr.readyState == 4 && xhr.status == 200) {
           // handle response.
          var data = xhr.responseText;

          if (data > 0){
            //console.log(data);
            var campo_id = $('<input />');
            campo_id.attr({'id':'idimg','name':'idimg','type':'hidden','value':data});
            $('#upload').append(campo_id);
            $('.messageOk').removeClass('hide');
            setTimeout(function(){ $('.messageOk').slideUp("slow")}, 3000);

          }
      }
  };

  fd.append('myFile', file);
  // Initiate a multipart/form-data upload
  xhr.send(fd);

}

// file selection
function FileSelectHandler(e) {
  var submitbutton = $id("submitbutton");
  // cancel event and hover styling
  FileDragHover(e);
  // fetch FileList object
  var files = e.target.files || e.dataTransfer.files;
  
  // process all File objects
  submitbutton.style.display = 'block';

  for (var i = 0, f; f = files[i]; i++) {
    ParseFile(f);
    SendData(f);
  }
  

}

// file drag hover
function FileDragHover(e) {
  e.stopPropagation();
  e.preventDefault();
  e.target.className = (e.type == "dragover" ? "hover" : "");
}

// getElementById
function $id(id) {
  return document.getElementById(id);
}

//
// output information
function Output(msg) {
  var m = $id("messages");
  m.innerHTML = msg + m.innerHTML;
}

function insertAfter(e,i){ 
        if(e.nextSibling){ 
            e.parentNode.insertBefore(i,e.nextSibling); 
        } else { 
            e.parentNode.appendChild(i); 
        }
    }