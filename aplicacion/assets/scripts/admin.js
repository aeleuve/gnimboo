$( document ).ready(function() {
	initOverlay();

	var path = window.location.pathname;
	var idcode = path.substr(path.lastIndexOf('/')+1);

	//codes list menu actions
	var listItems = $('.photogallery-nav').children();
	
	listItems.each(function(i,e){
		var listID = $(this).attr('id');
		if (listID == idcode){
			listItems.removeClass('active');
			$(this).addClass('active');
		}

	});

});

	//functions
	function buildHTML(file,text,date,id,idcodigo){
		var wrap	 = $('.grid-80'),
			wrapImg	 = $('<div></div>'),
			editwrap = $('<div></div>'),
			textwrap = $('<div></div>'),
			textIn 	 = $('<input />');
			dateIn 	 = $('<input />');
			datewrap = $('<div></div>'),
			grupo = $('<select></select>');
			path = base_url_uspath+'/',
			pic = $('<img />'),
			form = $('<form></form>'),
			uid = $('#uid').attr('value');

		//listado de grupos del usuario.
		//options del select según ábumes/códigos del usuario.
		
			$.post(base_url+'admin/images/ajaxUsersCodes', {uid:uid}, function(data){
					if (data != false){
						var items = JSON.parse(data);
						for (var i = 0; i < items.length; i++) {
						    var item = items[i];
						    var idoption = item.id,
						    	optiontext = item.codigo,
						    	optionsSelect = $('<option></option>');
						    
						    if (idcodigo == idoption) {
						    	optionsSelect.attr('selected', 'selected');	
						    };

						    optionsSelect.attr('value', idoption);
						    optionsSelect.html(optiontext);

						    grupo.append(optionsSelect);
						}
					}
				});

		form.attr({'id':'Edit','class':'form-horizontal'});
		wrapImg.attr({'class':'photoEdit','id':'wrap-img-edit'});
		wrapImg.attr({'class':'photoDetailEdit'});
		editwrap.attr({'id':'wrap-edit-details', 'class':'photoEdit form-group'});
		pic.attr({'src':path+file});

		wrapImg.html(pic);
		wrap.html('');
		wrap.append(wrapImg);

		//edit details.
		grupo.attr({'name':'grupo', 'id':'grupo'});
		textwrap.attr({'class':'text'});
		datewrap.attr({'class':'text'});
		textIn.attr({'id':'titulo', 'class':'form-control', 'type':'text', 'value':text});
		dateIn.attr({'id':'date', 'class':'form-control', 'type':'text', 'value':date});
		dateIn.attr({'id':'date', 'class':'form-control', 'type':'text', 'value':date});


		textwrap.append(textIn, dateIn, grupo);
		editwrap.append(textwrap);
		form.append(editwrap);
		wrapImg.after(form);

	}

	function buildForm(id){
		var labelDes = $('<label></label>'),
			labelDate = $('<label></label>'),
			labelGroup = $('<label></label>'),
			Save = $('<button></button>'),
			Cancel = $('<button></button>'),
			hidden = $('<input></input>');

			labelDes.html('Titulo');
			labelDate.html('Fecha');
			labelGroup.html('Codigo Cliente');

			Save.attr({'id':'edit-save'});
			Cancel.attr({'id':'edit-cancel'});
			hidden.attr({'id':'id-item', 'type':'hidden', 'value':id});

			Save.html('Guardar');
			Cancel.html('Dejarlo como está');

			$('#date').datepicker( { dateFormat: "yy-mm-dd" } );

			$('.text :first-child').after(labelDate);
			$('.text :first-child').before(labelDes);
			$('.text :last-child').before(labelGroup);
			$('.text :last-child').after(hidden);
			$('#wrap-edit-details').after(Save);

			pressButton(Save, Cancel);
	}

	function pressButton(S,C){
		var saveme = $(S),
			cancelme = $(C);

		saveme.click(function(){
			var titulo = $('#titulo').val(),
				fecha = $('#date').val(),
				id = $('#id-item').val(),
				grupo = $('#grupo').val();


				$.post(base_url+'admin/images/SaveEdition', {titulo:titulo, fecha:fecha, id:id, grupo:grupo}, function(data){
		
					if (data == true){
						$().redirect(base_url+"admin/main/menu", {'saved': 'true'});
					}
				});

		});

		cancelme.click(function(){
			window.location.replace(base_url+'admin/main/menu');
		});
	};

	function initOverlay(){
	var overlay = $('.overlay');
	var remove = $('.remove');
	var edit = $('.edit');
	var message = $('.messageOK');

	//quit fondocabecera from admin panel photo.
	$('.fondoCabecera').remove();

	overlay.mouseover(function(){
		$(this).css({'cursor':'default', 'opacity':'100'});
	});

	overlay.mouseout(function(){
		$(this).css({'cursor':'default', 'opacity':'0'});

	});

	remove.click(function(){
		var item = $(this).prev().html(),
			del = window.confirm ('¿Eliminar '+item+' ?'),
			square = $(this).parent().parent();
			id = $(this).parent().prev().attr('rel');


		if (del == true){
			$.post(base_url+"admin/images/ajaxDeleteImage", {id:id}, function(data){
				if (data == true){
					square.remove();
				}
    		});
		}

	});

	edit.click(function(){
		var item = $(this).next().html(),
			id = $(this).parent().prev().attr('rel');

		$.post(base_url+"admin/images/ajaxEdit", {id:id}, function(data){
			var itemsRequest = data;
			var items = JSON.parse(itemsRequest);

			for (var i = 0; i < items.length; i++) {
			    var item = items[i];
			    var file = item.nombre,
			    	text = item.descripcion,
			    	date = item.modified,
			    	idcodigo = item.id_codigo;

			}
			//build the html.			
			buildHTML(file,text,date,id,idcodigo);
			buildForm(id);
		});
	});

	if (message.length > 0){
		setTimeout(function(){
			message.slideUp("slow");
			}, 3000);		
	}		
	}

