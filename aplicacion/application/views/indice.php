<html>
<head>
	<title><?php echo $tituloPagina ?></title>
	<script type="text/javascript">var base_url = '<?= base_url() ?>'; </script>
	<script type="text/javascript">var base_url_uspath = "<?= base_url().'assets/img/files/'.strtolower(replaceAccent($this->session->userdata['firstName'].$this->session->userdata['lastName'].'_'.$this->session->userdata['id'])) ?>"; </script>
<?php
 if (isset($css)){
	if (is_array($css)){
		foreach ($css as $css_item) echo '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/'.$css_item.'" />';
	}else{
		echo '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/'.$css.'" />';
	}
}

 if (isset($js)){
 	if (is_array($js)){
 		foreach ($js as $js_item) echo '<script type="text/javascript" src="'.base_url().'assets/scripts/'.$js_item.'" /></script>';
 	}else{
 		echo '<script type="text/javascript" src="'.base_url().'assets/scripts/'.$js.'" ></script>';
 	}
}
?>

</head>
<body>
<div class="fondoCabecera">
	<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/img/logo.jpg" style="margin:auto;"></a>
</div>
<div id="main-wrap">
	<?php $this->load->view($vistas['cuerpo']);?>
</div>


</body>
</html>