<?php if (is_array($data)): ?>
<div id="ficha">
		<div class="derecha descarga_txt">¡ Elige el tamaño para descargar !</div>
		<div id="img_detail" class="shadow_detail"><img src="<?php echo base_url(); ?>assets/scripts/resize.php?w=300&img=/assets/img/files/<?php echo $data[0]['nombre']; ?>" /></div>
		<div id="prices_box">
			<table id="ficha_data">
				<thead>
					<tr>
						<td colspan="2" class="white">Size</td>
						<td class="white">Resolution</td>
						<td class="aquamarina">Price</td>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($data as $key => $val):?>
					</tr>
						<td><input type="radio" name="size" value="" /> <?php echo $val['path']; ?></td>
						<td> <?php echo $val['resolution']; ?> </td>
						<td> <?php echo $val['size']; ?></td>
						<td> <?php echo $val['precio']; ?></td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>

		<div class="clear"></div>
</div>

<?php else: ?>
	<div>
		<?php echo $data; ?>
	</div>
<?php endif; ?>