<div class="main-nav">
  <div class="left"><img src="<?php echo base_url()?>assets/img/logo-small.png"></div>
  <div class="config right">
      <div class="right Login_img"><img src="<?php echo base_url()?>assets/img/authors/<?php echo $userData['picture']; ?>"></div>
      <div class="right dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $userData['name']; ?><span class="caret"></span></a>
         <input type="hidden" id="uid" value="<?= $userData['id']; ?>" />
         <ul class="dropdown-menu" role="menu">
            <li><a href="#">Preferences</a></li>
            <li><a href="<?php echo base_url() ?>admin/main/upload">Upload</a></li>
            <li><a href="<?php echo base_url() ?>/admin/main">Pics admin</a></li>
            <li class="divider"></li>
            <li><a href="<?php echo base_url() ?>admin/main/logout_user">Logout</a></li>
          </ul>
      </div>
  </div>
	<div class="clear"></div>
</div>
<div class="grid-container">
  <div class="grid-10 left-nav">
  	<ul class="photogallery-nav nav nav-pills nav-stacked">
    <?php foreach ($codes as $c): ?>
    	 <li id="<?= $c->id; ?>"><a href="<?= base_url(); ?>admin/main/menu/<?= url_title(strtolower($c->codigo)); ?>/<?= $c->id; ?>"><?= $c->codigo; ?></a></li>
    <?php endforeach; ?>
        <li class="active"><a href="<?= base_url(); ?>admin/main/menu">Todas las imágenes</a></li>
    </ul>
  </div>
  <!-- container -->
  <div class="grid-80 center-nav">
  <?php if (isset($message)):?>
      <div class="messageOK"><?=$message;?></div>
  <?php endif;?>
    <?php if (isset($message_err)):?>
      <div class="message_err"><?=$message_err;?></div>
  <?php endif;?>
  	<ul class="photos selectable">
		<?php foreach ($images as $i): ?>
			<li class="thumbnail_nav" style="width: 168px; height: 168px;">
				<canvas id="canvas_<?= $i->id; ?>" width=168 height=168 class="thumbs" rev=<?= $i->nombre; ?> rel="<?= $i->id; ?>"></canvas>
        <div class="overlay">
          <div class="shadow"></div>
          <a class="button edit small" href="#"> Editar</a>
          <h4><?= $i->descripcion;?></h4>
          <a class="button remove small" href="#"> Borrar </a>
        </div>
			</li>
		<?php endforeach; ?>
	</ul>
  </div>
  <!-- container ends -->
  <div class="grid-10 right-nav">
    Sort by
  </div>
</div>