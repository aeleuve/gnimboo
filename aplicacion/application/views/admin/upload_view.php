<?php  echo $this->template->widget("navigation"); ?>
<div class="messageOk hide">Imagen guardada ! </div>
<form id="upload" action="<?= base_url();?>admin/upload/" method="POST" enctype="multipart/form-data">
<fieldset>
<input type="hidden" id="MAX_FILE_SIZE" name="MAX_FILE_SIZE" value="300000" />

<div id="contenDrag">
	<label for="fileselect">Files to upload:</label>
	<input type="file" id="fileselect" name="fileselect[]" multiple="multiple" />
	<div id="filedrag">or drop files here</div>
</div>

<div id="codigoContent">
	<label for = "codigos">Elige código: </label>
	<select id="codigos" name="codigos">
			<option selected="selected" value="0">Elige código</option>
		<?php foreach ($codes as $c): ?>
			<option value="<?= $c->id;?>"><?= $c->codigo;?></option>
		<?php endforeach; ?>
	</select>

</div>

<div>
	<label for="descripcion">Título</label>
	<input type="text" name="descripcion" id="descripcion" />
</div>


<div id="submitbutton">
	<button type="submit">Upload Files</button>
</div>

</fieldset>
<div id="messages">
<p></p>
</div>
</form>

<div id="messages_err" class="alert-error"></div>