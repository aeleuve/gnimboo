<?php foreach ($images as $i): ?>
<li class="thumbnail_nav" style="width: 168px; height: 168px;">
	<canvas id="canvas_<?= $i->id; ?>" width=168 height=168 class="thumbs" rev=<?= $i->nombre; ?> rel="<?= $i->id; ?>"></canvas>
	<div class="overlay">
	  <div class="shadow"></div>
	  <a class="button edit small" href="#"> Editar</a>
	  <h4><?= $i->descripcion;?></h4>
	  <a class="button remove small" href="#"> Borrar </a>
	</div>
</li>
<?php endforeach; ?>
