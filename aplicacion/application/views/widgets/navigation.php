<div class="main-nav">
  <div class="left"><img src="<?php echo base_url()?>assets/img/logo-small.png"></div>
  <div class="config right">
      <div class="right Login_img"><img src="<?php echo base_url()?>assets/img/authors/<?= $userData['picture']; ?>"></div>
      <div class="right dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $userData['name']; ?><span class="caret"></span></a>
         <ul class="dropdown-menu" role="menu">
            <li><a href="#">Preferences</a></li>
            <li><a href="<?php echo base_url() ?>admin/main/upload">Upload</a></li>
            <li><a href="<?php echo base_url() ?>/admin/main">Pics admin</a></li>
            <li class="divider"></li>
            <li><a href="<?php echo base_url() ?>admin/main/logout_user">Logout</a></li>
          </ul>
      </div>
  </div>
	<div class="clear"></div>
</div>