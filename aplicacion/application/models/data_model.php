<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

	class data_model extends CI_Model {
		function __construct()
		{
			parent::__construct();
		}

		//función que devuelve las imágenes de un álbum código de un usuario
		function getImagesByCodeUser($id, $slug){
			
			$this->db->select('b.id, nombre, descripcion');
			$this->db->where('cu.id_user',$id);
			$this->db->where('c.slug',$slug);

			$this->db->join('bancodigo bc', 'bc.id_banco = b.id','inner');
			$this->db->join('codigos c', 'bc.id_codigo = c.id','inner');
			$this->db->join('codigouser cu', 'cu.id_codigo = c.id','inner');

			$query=$this->db->get('Banco b');

			if ($query->num_rows() > 0) return $query->result();
			else return false;	
		}

		function getUserCodes($id)
		{
			$this->db->where('cu.id_user',$id);
			$this->db->join('codigouser as cu', 'cu.id_codigo = c.id', 'inner');
			$query=$this->db->get('codigos as c');

			if ($query->num_rows() > 0) return $query->result();
			else return false;
		}

		function listaCodigos(){
			$lista = array();
			$query = $this->db->query("select * from codigos");
			
			if ($query->num_rows() > 0){
				foreach ($query->result_array() as $row)
				{
					$lista[] = $row["codigo"];
				}

				return $lista;
			}else return false;
		}

		function getCodes(){
			$query = $this->db->get('codigos');
			if ($query->num_rows() > 0) return $query->result();
			else return false;

		}

		function getImage($id){
			$rows = array();
			$this->db->join('bancodigo as bc', 'bc.id_banco = Banco.id');
			$this->db->join('codigos as c', 'c.id = bc.id_codigo', 'left');
			$this->db->where('Banco.id', $id);
			$q = $this->db->get('Banco');

			if ($q->num_rows() > 0){
				foreach ($q->result_array() as $row) {
					$row['modified'] = date("Y/m/d", strtotime($row['modified']));
					$rows[] = $row;
				}
				return $rows;
			}
			else return false;
		}

		function getImages($c){
			$this->db->select('Banco.id, nombre, descripcion');
			$this->db->join('bancodigo as bc', 'bc.id_banco = Banco.id', 'left');
			$this->db->join('codigos as c', 'c.id = bc.id_codigo', 'left');
			$this->db->where('c.codigo', $c);
			$query=$this->db->get('Banco');		

			if ($query->num_rows() > 0) return $query->result();
			else return FALSE;

		}

		function getDetail($id){
			$this->db->select('nombre, sz.size, sz.path, sz.precio, sz.resolution');
			$this->db->join('size as sz', 'sz.banco_id = Banco.id', 'inner');
			$this->db->where('Banco.id', $id);
			$query = $this->db->get('Banco');

			if ($query->num_rows() > 0) return $query->result_array();
			else return FALSE;
		}

		function eliminaImagen($id){
			$this->db->trans_start();
				$this->db->delete('Banco', array('id' => $id));
				$this->db->delete('bancodigo', array('id_banco' => $id));
			$this->db->trans_complete();

			if ($this->db->trans_status() === TRUE)
			{			
			    return true;
			}else return false;
		}

		function saveEdit($id,$titulo,$fecha,$grupo){
			//trans start para guardar id de grupo en bancodigo.
			$data = array('descripcion'=>$titulo, 'modified'=>$fecha);			
			$this->db->trans_start();	

				$this->db->where('id_banco', $id);
				$this->db->update('bancodigo', array('id_codigo'=>$grupo));

				$this->db->where('id', $id);
				$q = $this->db->update('Banco', $data);

			$this->db->trans_complete();
			if ($this->db->trans_status() === TRUE) return true;
			else return false;
			//return $this->db->_error_number();
		}

		function newImage($data, $async=false){
			
			if ($async){
				$this->db->insert('Banco', array('nombre'=>$data)); 
				return $this->db->insert_id();	
			}else{
				$img_data = array(
					'nombre' => $data['imageName'],
					'created' => date("Y-m-d h:i:s"),
					'modified' => date("Y-m-d h:i:s"),
					'descripcion' => $data['titulo']
				);

				$this->db->trans_start();
					$this->db->insert('Banco', $img_data);
					$id_banco = $this->db->insert_id();

					$this->db->set('id_banco', $id_banco);
					$this->db->set('id_codigo', $data['code']);
					$this->db->insert('bancodigo');

				$this->db->trans_complete();
				if ($this->db->trans_status() === TRUE) return true;
				else return false;
			}
		}

		function aj_InsertImg($data){
			// array(5) { ["code"]=> string(1) "2" ["titulo"]=> string(2) "fa" ["tipo"]=> string(10) "image/jpeg" ["size"]=> string(6) "109591" ["idimg"]=> string(2) "22" }
			$img_data = array(
					'created' => date("Y-m-d h:i:s"),
					'modified' => date("Y-m-d h:i:s"),
					'descripcion' => $data['titulo']
				);

			$this->db->trans_start();
				$this->db->where('id', $data['idimg']);
				$this->db->update('Banco', $img_data); 

				$this->db->set('id_banco', $data['idimg']);
				$this->db->set('id_codigo', $data['code']);
				$this->db->insert('bancodigo');

				$this->db->trans_complete();
			if ($this->db->trans_status() === TRUE) return true;
			else return false;
		}

}

?>