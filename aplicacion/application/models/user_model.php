<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

	class user_model extends CI_Model {
		function __construct()
		{
			parent::__construct();
		}

var $details;

function validate_user( $email, $password ) {
    // Build a query to retrieve the user's details
    // based on the received username and password
    $this->db->from('users');
    $this->db->where('email',$email );
    $this->db->where( 'password', sha1($password) );
    $login = $this->db->get()->result();
    // The results of the query are stored in $login.
    // If a value exists, then the user account exists and is validated
    if ( is_array($login) && count($login) == 1 ) {
        // Set the users details into the $details property of this class
        $this->details = $login[0];
        // Call set_session to set the user's session vars via CodeIgniter
        $this->set_session();
        return true;
    }

    return false;
}

function set_session() {
    // session->set_userdata is a CodeIgniter function that
    // stores data in a cookie in the user's browser.  Some of the values are built in
    // to CodeIgniter, others are added (like the IP address).  See CodeIgniter's documentation for details.
    $this->session->set_userdata( array(
            'id'=>$this->details->id,
            'name'=> $this->details->firstName . ' ' . $this->details->lastName,
            'email'=>$this->details->email,
            'active'=>$this->details->active,
            'groups_id'=>$this->details->groups_id,
            'firstName'=>$this->details->firstName,
            'lastName'=>$this->details->lastName,
            'picture'=>$this->details->picture,
            'isLoggedIn'=>true
        )
    );
}	

    function fill_session_data(){
      $data['id'] = $this->session->userdata('id');
      $data['email'] = $this->session->userdata('email');
      $data['firstName'] = $this->session->userdata('firstName');
      $data['lastName'] = $this->session->userdata('lastName');
      $data['picture'] = $this->session->userdata('picture');
      $data['name'] = $this->session->userdata('name');
      // $data['id'] = $this->session->userdata('idu');

      return $data;
    }

    function getGalleryCodes($id){
    	$this->db->select('cc.id, codigo');
    	$this->db->join('codigouser cu', 'cc.id = cu.id_codigo', 'left');
    	$this->db->where('cu.id_user',$id);
    	$query = $this->db->get('codigos cc');

		if ($query->num_rows() > 0) return $query->result();
		else return FALSE;
    }


    function getAllFromUser($id){
    	$id_codes = array();
    	$this->db->select('cc.id, codigo');
    	$this->db->join('codigouser cu', 'cc.id = cu.id_codigo', 'left');
    	$this->db->where('cu.id_user',$id);
    	$queryCodes = $this->db->get('codigos cc');

    	foreach ($queryCodes->result() as $c) {
    		$id_codes[]=$c->id;
    	}

    	$this->db->select('b.id, nombre, descripcion');
    	$this->db->join('bancodigo bc', 'b.id = bc.id_banco', 'left');
    	$this->db->where_in('bc.id_codigo', $id_codes);
    	$query = $this->db->get('Banco b');

		if ($query->num_rows() > 0) return $query->result();
		else return FALSE;
    }

}
?>