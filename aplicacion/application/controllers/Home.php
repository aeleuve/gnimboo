<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->init();
	}

	function init(){
		$this->load->helper(array('form', 'url'));
		$this->load->model('data_model');
		setlocale(LC_TIME, 'es_ES.UTF-8');
	}	

	function index(){
		if ($this->session->userdata('dcode')) $this->session->unset_userdata('dcode');

		$cabecera['css'] = 'styleSass.css';
		$cabecera['js'] = array ('jquery1-11-1.js', 'javascript.js');
		$cabecera['tituloPagina'] = 'Gnimboo | Your best travel photos! ';		
		$data['attrib'] = array('class' => 'formapparel', 'id'=>'form_download');
		$data['destino'] = 'home/descarga';


		$salida = $this->load->view('top_view', $cabecera);
		$salida .= $this->load->view('home_view', $data);
		$salida .= $this->load->view('bottom_view');

		echo $salida;
	}



	function descarga(){
		$code_promo = $this->input->post('d_code');
		$sesion_ini = $this->session->userdata('dcode');

		$cabecera['css'] = array('jquery-ui.css','styleSass.css','bootstrap.css');
		$cabecera['js'] = array ('jquery1-11-1.js', 'jquery-ui.js', 'javascript.js');				
		$cabecera['tituloPagina'] = 'Gnimboo | Your best travel photos! ';				

		$salida  = $this->load->view('top_view', $cabecera);

		if ($code_promo != false){
			$this->session->unset_userdata('d_code');
			$codigo = $this->input->post('d_code');
			$this->session->set_userdata('dcode',$code_promo);
		}else{
			$code_promo = $this->session->userdata('dcode');
		}

		$existe = $this->_comparaCodigo($code_promo);

			if ($existe == true){
			// consulta las imagenes que tiene el código.
				$getImages = $this->data_model->getImages($code_promo);
				$Images['Images'] = $getImages;
				$salida .= $this->load->view('list_view', $Images);
		 	}else{
				$salida .= $this->load->view('err_nopic_view');	
			}

			$salida .= $this->load->view('bottom_view');

		echo $salida;
	}

	function detalle(){
		$id = $_POST['id'];

		$getDetail = $this->data_model->getDetail($id);

		if ($getDetail != FALSE) $data = $getDetail;
		else $data = 'No existen datos';

		$datos['data'] = $data;
		$salida = $this->load->view('detail', $datos);

		echo $salida;
	}

	function _comparaCodigo($c){
		$codigos = $this->data_model->listaCodigos();

		if ($codigos !== false){
			foreach ($codigos as $kc ) {
				if ($kc == $c){
					return true;
				}else{
					return false;
				}
			}
		}else return false;

	}

}

?>