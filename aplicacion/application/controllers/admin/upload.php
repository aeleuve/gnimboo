<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class upload extends CI_Controller {

	function index(){
	$this->load->helper('url');
	if( $this->session->userdata('isLoggedIn') ) {	
		$this->load->model(array('data_model','user_model'));
		$this->load->helper(array('url','accentreplace'));

		$data['code'] = $this->input->post('codigos');
		$data['titulo'] = $this->input->post('descripcion');
		$data['tipo'] = $this->input->post('tipo');
		$data['size'] = $this->input->post('f_size');
		$folder = strtolower($this->_replace_accents($_SERVER['DOCUMENT_ROOT'].'/assets/img/files/'.$this->session->userdata('firstName').$this->session->userdata('lastName').'_'.$this->session->userdata('id')));


		if (isset($_POST['idimg'])){
			
			$data['idimg'] = $this->input->post('idimg');
			$update = $this->data_model->aj_InsertImg($data);

			if ($update) $data['message'] = 'Nueva imagen guardada correctamente.';
    		else $data['message_err'] = 'Imagen no se pudo guardar en BD';
		}

		if (!empty($_FILES['myFile']['tmp_name'])){ //guardamos imagen por envío xhr y devolvemos respuesta
			
			if (!file_exists($folder) && !is_dir($folder)) {
  			  mkdir($folder);     
			}

			$tmp_name = $_FILES['myFile']['tmp_name'];
			$name = $_FILES['myFile']['name'];
    		move_uploaded_file($tmp_name, "$folder/$name");

    		if ($_FILES['myFile']['error'] == UPLOAD_ERR_OK){
    			$save = $this->data_model->newImage($name, $async=true);
    		}else{
    			$save = -1;
    		}

    		echo $save;
    		
		} 

		if (!empty($_FILES['fileselect']['tmp_name'][0])) {
			if (!file_exists($folder) && !is_dir($folder)) {
  			  mkdir($folder);     
			}	 
    		
			foreach ($_FILES["fileselect"]["error"] as $key => $error) { //guardamos imagen por input
			    if ($error == UPLOAD_ERR_OK) {
			    	$tmp_name = $_FILES['fileselect']['tmp_name'][$key];
			    	$name = $_FILES['fileselect']['name'][$key];
    				move_uploaded_file($tmp_name, "$folder/$name");
    				$data['imageName'] = $name;
    				//guardar en base de datos.
    				$save = $this->data_model->newImage($data);

    				if ($save) $data['message'] = 'Nueva imagen guardada correctamente.';
    				else $data['message_err'] = 'Imagen no se pudo guardar en BD';

    			}else{

    				switch ($_FILES['fileselect']['error']) {
				        case UPLOAD_ERR_NO_FILE:
				            // throw new RuntimeException('No file sent.');
				        	$data['message_err'] = 'Archivo no enviado';
				        case UPLOAD_ERR_INI_SIZE:
				        case UPLOAD_ERR_FORM_SIZE:
				            // throw new RuntimeException('Exceeded filesize limit.');
				        	$data['message_err'] = 'La imagen excede el tamaño especificado';
				        default:
				            // throw new RuntimeException('Unknown errors.');
				        	$data['message_err'] = 'No se ha podido guardar la imagen. Intente de nuevo.';
				    }

    			}
			}
		}
		
			 

		if (!empty($_FILES['fileselect']['tmp_name'][0]) || isset($_POST['idimg'])) {	 
			$user_id = $this->session->userdata('id');
		    $group = $this->session->userdata('groups_id');

			$data['css'] = array('bootstrap.css', 'styleSass.css', 'jquery-ui.css');
			$data['js'] = array ('jquery1-11-1.js','canvas.js', 'jquery.redirect.min.js', 'admin.js', 'jquery-ui.js','bootstrap.min.js');
			$data['tituloPagina'] = 'Gnimboo | User Menu';

			 $data['userData'] = $this->user_model->fill_session_data();
			 $data['codes'] = $this->user_model->getGalleryCodes($user_id);
			 $data['images'] = $this->user_model->getAllFromUser($user_id);
			 $data['saludo'] = 'Hola';
			 $data['vistas'] = array('cuerpo'=>'admin/menu_view');
			 $this->load->view('indice', $data);		
		 }
	}else{
		redirect('admin/main/logout_user', 'refresh');
	}
}
	 function _replace_accents($string) 
	{ 
  		return str_replace( array('à','á','â','ã','ä', 'ç', 'è','é','ê','ë', 'ì','í','î','ï', 'ñ', 'ò','ó','ô','õ','ö', 'ù','ú','û','ü', 'ý','ÿ', 'À','Á','Â','Ã','Ä', 'Ç', 'È','É','Ê','Ë', 'Ì','Í','Î','Ï', 'Ñ', 'Ò','Ó','Ô','Õ','Ö', 'Ù','Ú','Û','Ü', 'Ý'), array('a','a','a','a','a', 'c', 'e','e','e','e', 'i','i','i','i', 'n', 'o','o','o','o','o', 'u','u','u','u', 'y','y', 'A','A','A','A','A', 'C', 'E','E','E','E', 'I','I','I','I', 'N', 'O','O','O','O','O', 'U','U','U','U', 'Y'), $string); 
	} 
}

?>