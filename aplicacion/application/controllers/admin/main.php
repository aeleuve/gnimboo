<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class main extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->init();
	}	

	function init(){
		$this->load->helper(array('form', 'url','accentreplace','thumbs_helper'));
		$this->load->model(array('data_model','user_model'));
		setlocale(LC_TIME, 'es_ES.UTF-8');
	}		

	function index(){
		if( $this->session->userdata('isLoggedIn') ) {
		        redirect('/admin/main/menu');
		    } else {
		        $this->show_login(false);
		    }
	}

	function showImagesbyCode(){
		if ($this->session->userdata('isLoggedIn')){
			$slug = $this->uri->segment(4);
			$id = $this->session->userdata['id'];
			$user_id = $this->session->userdata('id');
			$group = $this->session->userdata('groups_id');

			$data['css'] = array('bootstrap.css', 'styleSass.css', 'jquery-ui.css');
			$data['js'] = array ('jquery1-11-1.js','canvas.js', 'jquery.redirect.min.js', 'admin.js', 'jquery-ui.js','bootstrap.min.js');
			$data['tituloPagina'] = 'Gnimboo:'.$slug;


			$data['userData'] = $this->user_model->fill_session_data();
			$data['codes'] = $this->user_model->getGalleryCodes($user_id);
			$data['images'] = $this->data_model->getImagesByCodeUser($id, $slug);
			$data['saludo'] = 'Hola';
			$data['vistas'] = array('cuerpo'=>'admin/menu_view');

			$this->load->view('indice', $data);

		}else{
			$this->show_login(false);
		}	
	}

	function show_login($show_error = false){
		$data['error'] = $show_error;
		$data['css'] = array('styleSass.css');
		$data['js'] = array ('jquery1-11-1.js', 'javascript.js', 'bootstrap.js');
		$data['tituloPagina'] = 'Gnimboo | Your best travel photos! ';		
		$data['vistas'] = array('cuerpo'=>'admin/login_view');

		$this->load->view('indice', $data);
	}

	function menu(){
		if ($this->session->userdata('isLoggedIn')){
			// Get some data from the user's session
		    $user_id = $this->session->userdata('id');
		    $group = $this->session->userdata('groups_id');

			$data['css'] = array('bootstrap.css', 'styleSass.css', 'jquery-ui.css');
			$data['js'] = array ('jquery1-11-1.js','canvas.js', 'jquery.redirect.min.js', 'admin.js', 'jquery-ui.js','bootstrap.min.js');
			$data['tituloPagina'] = 'Gnimboo | User Menu';

			if (isset($_POST['saved']) && $_POST['saved'] == 'true'){
		    	$data['message'] = 'Datos salvados correctamente';
		    }

		    $data['userData'] = $this->user_model->fill_session_data();
		    $data['codes'] = $this->user_model->getGalleryCodes($user_id);
		    $data['images'] = $this->user_model->getAllFromUser($user_id);
		    $data['saludo'] = 'Hola';
		    $data['vistas'] = array('cuerpo'=>'admin/menu_view');

		    $this->load->view('indice', $data);
		}else{
			$this->show_login(false);
		}

	}	

	function login_user(){
		$email = $this->input->post('email');
		$pass = $this->input->post('password');

		//Ensure values exist for email and pass, and validate the user's credentials
      	if( $email && $pass && $this->user_model->validate_user($email,$pass)) {
          // If the user is valid, redirect to the main view
          redirect('/admin/main/menu');
      	} else {
          // Otherwise show the login screen with an error message.
          $this->show_login(true);
      }
	}

	function upload(){
	if ($this->session->userdata('isLoggedIn')){
		$this->load->library('template');
		$this->load->model('data_model');
		$data['css'] = array('bootstrap.css', 'styleSass.css', 'jquery-ui.css');
		$data['userData'] = $this->user_model->fill_session_data();
		$data['tituloPagina'] = 'Upload / Gnimboo';
		$data['js'] = array ('jquery1-11-1.js', 'uploader.js' , 'bootstrap.min.js');
		$data['codes'] = $this->data_model->getCodes();

		$data['vistas'] = array('cuerpo'=>'admin/upload_view');
		$this->load->view('admin/indice', $data);
	}else{
			$this->show_login(false);
		}
	}

    function logout_user() {
      $this->session->sess_destroy();
      $this->index();
    }	
}

?>