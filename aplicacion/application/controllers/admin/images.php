<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class images extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->init();
	}	

	function init(){
		$this->load->model('data_model');
	}

	function ajaxDeleteImage(){
		echo $this->data_model->eliminaImagen($_POST['id']);
	}

	function ajaxEdit(){
		$array = $this->data_model->getImage($_POST['id']);
		echo json_encode($array);
	}

	function ajaxUsersCodes(){
		$codigos = $this->data_model->getUserCodes($_POST['uid']);
		echo json_encode($codigos);
	}

	function SaveEdition(){
		$titulo = $this->input->post('titulo');
		$fecha = $this->input->post('fecha');
		$id = $this->input->post('id');
		$grupo = $this->input->post('grupo');

		$c = $this->data_model->saveEdit($id,$titulo,$fecha,$grupo);
		echo $c;
		// if ($c == 0) echo true;
		// else echo false;
	}

	function ajaxShowImageCodes(){
		$id = $this->input->post('id');
		$c = $this->data_model->getImages($id);
		$data['images'] = $c;

		if ($c != FALSE) $this->load->view('admin/json_gallery_view', $data);
		else echo 'false';
	}

}
?>