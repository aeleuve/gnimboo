# ************************************************************
# Sequel Pro SQL dump
# Versión 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.20)
# Base de datos: gn_panel
# Tiempo de Generación: 2015-01-24 16:24:46 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla Banco
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Banco`;

CREATE TABLE `Banco` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `descripcion` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Banco` WRITE;
/*!40000 ALTER TABLE `Banco` DISABLE KEYS */;

INSERT INTO `Banco` (`id`, `nombre`, `created`, `modified`, `descripcion`)
VALUES
	(1,'_MG_1483.jpg','2014-08-10 13:09:38','2014-08-10 13:09:38','cxbv'),
	(12,'_MG_8271.jpg','2014-08-21 13:32:34','2014-08-21 13:32:34','ffdfds');

/*!40000 ALTER TABLE `Banco` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla bancodigo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bancodigo`;

CREATE TABLE `bancodigo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_banco` int(11) unsigned DEFAULT NULL,
  `id_codigo` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `bancodigo` WRITE;
/*!40000 ALTER TABLE `bancodigo` DISABLE KEYS */;

INSERT INTO `bancodigo` (`id`, `id_banco`, `id_codigo`)
VALUES
	(1,1,1),
	(2,12,2);

/*!40000 ALTER TABLE `bancodigo` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla codigos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `codigos`;

CREATE TABLE `codigos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `codigos` WRITE;
/*!40000 ALTER TABLE `codigos` DISABLE KEYS */;

INSERT INTO `codigos` (`id`, `codigo`)
VALUES
	(1,'helloworld'),
	(2,'holyshit');

/*!40000 ALTER TABLE `codigos` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla codigouser
# ------------------------------------------------------------

DROP TABLE IF EXISTS `codigouser`;

CREATE TABLE `codigouser` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned NOT NULL,
  `id_codigo` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `codigouser` WRITE;
/*!40000 ALTER TABLE `codigouser` DISABLE KEYS */;

INSERT INTO `codigouser` (`id`, `id_user`, `id_codigo`)
VALUES
	(1,3,1),
	(2,3,2);

/*!40000 ALTER TABLE `codigouser` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla foto_noticia
# ------------------------------------------------------------

DROP TABLE IF EXISTS `foto_noticia`;

CREATE TABLE `foto_noticia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `noticia_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_foto_noticia_noticia1_idx` (`noticia_id`),
  CONSTRAINT `fk_foto_noticia_noticia1` FOREIGN KEY (`noticia_id`) REFERENCES `noticia` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Volcado de tabla groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='grupo de usuarios';

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `name`, `created`, `modified`)
VALUES
	(1,'guests','2014-03-30 13:24:56','2014-03-30 13:24:56'),
	(2,'administrators','2014-03-30 13:25:15','2014-03-30 13:25:15');

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla noticia
# ------------------------------------------------------------

DROP TABLE IF EXISTS `noticia`;

CREATE TABLE `noticia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `cuerpo` varchar(100) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `noticia` WRITE;
/*!40000 ALTER TABLE `noticia` DISABLE KEYS */;

INSERT INTO `noticia` (`id`, `titulo`, `fecha`, `cuerpo`, `created`, `modified`)
VALUES
	(4,'noticia de prueba','2014-08-03','hola que tal','2014-08-05 16:26:17','2014-08-05 16:26:17');

/*!40000 ALTER TABLE `noticia` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla size
# ------------------------------------------------------------

DROP TABLE IF EXISTS `size`;

CREATE TABLE `size` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `banco_id` int(11) DEFAULT NULL,
  `size` float DEFAULT NULL,
  `path` varchar(400) DEFAULT NULL,
  `precio` decimal(4,2) DEFAULT NULL,
  `resolution` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `size` WRITE;
/*!40000 ALTER TABLE `size` DISABLE KEYS */;

INSERT INTO `size` (`id`, `banco_id`, `size`, `path`, `precio`, `resolution`)
VALUES
	(1,1,345.56,'small',1.10,'800x600'),
	(2,1,600,'medium',2.00,'1024x800'),
	(3,1,3000,'Large',4.00,'1920x1200');

/*!40000 ALTER TABLE `size` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(150) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `groups_id` int(11) NOT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `picture` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Usuarios del sistema';

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `password`, `active`, `created`, `modified`, `groups_id`, `firstName`, `lastName`, `picture`)
VALUES
	(1,'guest@dominio.com','fc171b78f04ec3786ad23dc0e5b9d8bec599dc5e',1,'2011-11-13 12:36:25','2012-02-27 11:38:28',1,NULL,NULL,NULL),
	(2,'admin@dominio.com','fc171b78f04ec3786ad23dc0e5b9d8bec599dc5e',1,'2011-11-13 12:36:54','2012-09-08 11:55:47',2,NULL,NULL,NULL),
	(3,'aeleuve@hotmail.com','99800b85d3383e3a2fb45eb7d0066a4879a9dad0',0,NULL,NULL,0,'Álvaro','Ramírez','alv_spain.jpg');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Usuarios
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Usuarios`;

CREATE TABLE `Usuarios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT '',
  `Apellidos` varchar(150) DEFAULT NULL,
  `Perfil` varchar(100) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Usuarios` WRITE;
/*!40000 ALTER TABLE `Usuarios` DISABLE KEYS */;

INSERT INTO `Usuarios` (`id`, `Nombre`, `Apellidos`, `Perfil`, `password`)
VALUES
	(1,'Administrador','gnimboo','Admin',NULL);

/*!40000 ALTER TABLE `Usuarios` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
